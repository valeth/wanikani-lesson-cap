# =======[ WKOF Guard ]=======

# coffeelint: disable=max_line_length
unless window.wkof?
  confirmed = confirm '''
    WaniKani Lesson Cap requires the WaniKani Open Framework.
    Do you want to be forwarded to the installation instructions?
  '''
  window.location.href = "https://community.wanikani.com/t/instructions-installing-wanikani-open-framework/28549" if confirmed
  return
# coffeelint: enable=max_line_length

# =======[ Globals ]=======

btn_continue = undefined
settings_dialog = undefined
default_settings =
  max_score: 100

srs_levels = [
  { stage: 1, rating: 6,    ident: 'apprentice1' },
  { stage: 2, rating: 3,    ident: 'apprentice2' },
  { stage: 3, rating: 1.04, ident: 'apprentice3' },
  { stage: 4, rating: 0.51, ident: 'apprentice4' },
  { stage: 5, rating: 0.14, ident: 'guru1' },
  { stage: 6, rating: 0.07, ident: 'guru2' },
  { stage: 7, rating: 0.03, ident: 'master' },
  { stage: 8, rating: 0.01, ident: 'enlightened' }
]

# =======[ Utility functions ]=======

log =
  info: (message) -> console.log("[WKLC] " + message)
  error: (message) -> console.error("[WKLC] " + message)
  debug: (message) -> console.log("[WKLC] " + message) if debugging
  inspect: (thing) -> console.debug(thing) if debugging

# Format a given string into "titleized" format.
# Example:
#   someIdentifier1 -> Some Identifier 1
titleize = (string) ->
  tmp = string[0].toUpperCase()
  for char in string.slice(1)
    if char is char.toUpperCase()
      tmp += " #{char}"
    else
      tmp += char
  tmp

round2 = (num) ->
  Math.round(num * 100) / 100

get_or_wait = (key, callback) ->
  value = wkof.get_state(key)
  return callback(value) if value?
  wkof.wait_state key, "*", (value, _) ->
    return callback(value)

# =======[ Rendering ]=======

start_button = (action) ->
  btn_start = $("#start-session > a")
  return unless btn_start[0]?
  lesson_queue_count = $("#lesson-queue-count")
  switch(action)
    when "disable"
      log.info("Disabling lesson start button...")
      btn_start.addClass("wklc-force-hidden")
      lesson_queue_count.addClass("wklc-lesson-count")
    when "enable"
      log.info("Enabling lesson start button...")
      btn_start.removeClass("wklc-force-hidden")
      lesson_queue_count.removeClass("wklc-lesson-count")

continue_button = (action) ->
  btn_continue = $("#lesson-ready-continue") unless btn_continue?
  return unless btn_continue[0]?
  switch(action)
    when "disable"
      log.info("Disabling lesson continue button...")
      btn_continue.remove()
    when "enable"
      log.info("Enabling lesson continue button...")
      $("#screen-lesson-ready .btn-set").append(btn_continue)

render_all = ->
  get_items (score, stats) ->
    render_results(score, stats)

    batch_size = $.jStorage.get("l/batchSize")
    max_score = settings("max_score")
    remaining_score = max_score - score
    score_per_item = batch_size * (srs_levels[0].rating / 2)
    batches = Math.floor(remaining_score / score_per_item)

    start_button("enable") if batches >= 1
    continue_button("enable") if batches >= 2
    render_available(batch_size, batches)

render_results = (score, stats) ->
  return unless $("#lessons-summary")[0]?
  log.debug("Rendering results...")
  wklc = $(".wklc")
  wklc.remove() if wklc[0]?
  max_score = settings("max_score")
  wklc_items = stats.reduce (acc, stat, index) ->
    acc += """
      <li class='wklc-item wklc-#{srs_levels[index].ident}'
          title='#{titleize(srs_levels[index].ident)}'>
        <span>#{round2(stat.count)}</span>
        <span>(#{round2(stat.score)})</span>
      </li>
    """
  , ""
  $("#lessons-summary").prepend $ """
    <div class="wklc">
      <ul class="wklc-items">#{wklc_items}</ul>
      <div class='wklc-right'>
        <div class='wklc-item wklc-score'>
          <span>Score:</span>
          <span>#{round2(score)}</span> / <span>#{max_score}</span>
        </div>
        <button class='wklc-item wklc-btn-settings'
                title='Settings'>⚙</button>
      </div>
    </div>
  """

render_available = (batch_size, batches) ->
  return unless $("#lessons-summary")[0]?
  allowed_lessons = $(".wklc-allowed-lessons")
  allowed_lessons.remove() if allowed_lessons[0]?
  lessons_queue_count = +$("#lesson-queue-count").text()
  return if lessons_queue_count is 0 and not debugging
  title = """
    This is the amount of batches of #{batch_size}
    lessons you can do without exceeding the max score.
    """
  allowed_lessons = $ """
  <span class='wklc-allowed-lessons' title='#{title}'>
    #{batches + ' Available'}
  </span>
  """
  if batches < 1
    allowed_lessons.addClass("disabled")
    allowed_lessons.attr "title", """
      Lessons button disabled because you can do
      less than one session before exceeding max score.
      """
  $("#start-session").append(allowed_lessons)

# ======[ API Requests and Calculations ]=======

get_items = (callback) ->
  wkof.ItemData.get_items
    wk_items:
      options:
        assignments: true
      filters:
        srs: { value: ["lock", "burn"], invert: true },
        level: "1 to +0" # from first level to current
  .then (items) ->
    [score, stats] = calculate_score(items)
    callback(score, stats)

calculate_score = (items) ->
  stats = srs_levels.map(-> count: 0, score: 0.0)
  items.forEach (cur) ->
    stage = cur.assignments.srs_stage - 1
    return if stage < 0 or stage > 7
    stats[stage].count += 1
    stats[stage].score = (stats[stage].count * srs_levels[stage].rating) / 2
  wkof.set_state("wklc.stats", stats)

  total_score = stats.reduce (acc, cur) ->
    acc += round2(cur.score)
  , 0
  wkof.set_state("wklc.current_score", total_score)

  return [total_score, stats]

# =======[ Setup ]=======

update = ->
  # need to disable buttons on every update again
  start_button("disable")
  continue_button("disable")
  render_all()

install_menu = ->
  open_settings = ->
    settings_dialog.open() if settings_dialog?

  wkof.Menu.insert_script_link
    script_id: "wklc"
    submenu: "Settings"
    title: "WaniKani Lesson Cap"
    on_click: open_settings

  $(document).on("click", ".wklc-btn-settings", open_settings)

settings = (key) ->
  wkof.settings.wklc[key]

install_settings = ->
  save_settings = ->
    wkof.Settings.save("wklc")
    update()
    log.info("Settings saved")

  wkof.Settings.load("wklc", default_settings)

  settings_dialog = new wkof.Settings
    script_id: "wklc"
    title: "WaniKani Lesson Cap"
    on_save: save_settings
    settings:
      pg_general:
        type: "page"
        label: "General"
        content:
          max_score: { type: "number", label: "Max score" }

$(document).on("click", "#batch-items .active-quiz", update)
$(document).on("click", "#quiz-ready-continue", update)
$(document).ready ->
  modules = "Apiv2, ItemData, Menu, Settings"
  # coffeelint: disable=max_line_length
  wkof.load_css("https://raw.githubusercontent.com/valeth/wanikani-lesson-cap/wkof/compiled/style.css")
  # coffeelint: enable=max_line_length
  wkof.include(modules)
  wkof.ready("Menu").then(install_menu)
  wkof.ready("Settings").then(install_settings)
  wkof.ready(modules).then(update)
