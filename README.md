[![pipeline status](https://gitlab.com/valeth/wanikani-lesson-cap/badges/master/pipeline.svg)](https://gitlab.com/valeth/wanikani-lesson-cap/commits/master)
[![userscript](https://img.shields.io/badge/dynamic/json.svg?label=UserScript&colorB=&prefix=v&suffix=&query=$.version&uri=https%3A%2F%2Fgitlab.com%2Fvaleth%2Fwanikani-lesson-cap%2Fraw%2Fmaster%2Fpackage.json)](https://gitlab.com/valeth/wanikani-lesson-cap/raw/master/compiled/main.user.js)

# WaniKani Lesson Cap

This script limits your lessons by calculating a score from your current review queue.

## Install
Requires a script extension like [Tampermonkey]( https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)

* [Greasy Fork](https://greasyfork.org/en/scripts/34927-wanikani-lesson-cap)
* [GitHub](https://github.com/valeth/wanikani-lesson-cap/raw/master/main.user.js)

---
[Original Script](https://greasyfork.org/en/scripts/32173-wk-anti-burnout) by [finnra](https://www.wanikani.com/users/finnra)