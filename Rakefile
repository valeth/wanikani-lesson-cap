require 'erb'
require 'json'
require 'sass/plugin'

@development = false

SRCDIR = './src'.freeze
TMPDIR = './tmp'.freeze
CMPDIR = './compiled'.freeze

# BEGIN: for ERB template
def script_version
  json = JSON.load(open('package.json'))
  ver = json["version"]
  ver += "-dev#{Time.now.to_i}" if @development
  ver
end

def javascript
  open("#{TMPDIR}/main.js", 'r').read
end

def js_debugging
  "const debugging = #{@development ? 'true' : 'false'};"
end
### END

task :compile do
  sh "coffee -c -o #{TMPDIR}/ #{SRCDIR}/main.coffee"
end

task template: :compile do
  dev = "-dev" if @development
  open("#{SRCDIR}/main.user.js.erb", 'r') do |template|
    erb = ERB.new(template.read).result
    open("#{CMPDIR}/main#{dev}.user.js", 'w') do |userscript|
      userscript.write(erb)
    end
  end
end

task dev: :template do
  open("#{SRCDIR}/dev.user.js.erb", "r") do |template|
    erb = ERB.new(template.read).result
    open("#{TMPDIR}/dev.user.js", "w") do |devscript|
      devscript.write(erb)
    end
  end
end

task :build, [:env] do |_task, args|
  @development = args[:env]&.match?(/dev.*/) || false
  Rake::Task[:template].invoke
end

task :styles do
  Sass::Plugin.options[:template_location] = SRCDIR
  Sass::Plugin.options[:css_location] = CMPDIR
  Sass::Plugin.options[:sourcemap] = :none
  Sass::Plugin.update_stylesheets
end

task default: :build
